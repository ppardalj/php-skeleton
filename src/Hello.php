<?php

namespace Ppardalj\PhpSkeleton;

class Hello
{
    public function salute(string $name): string
    {
        return "Hello, $name!";
    }
}
